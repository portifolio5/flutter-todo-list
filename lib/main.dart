import 'dart:convert';

import 'package:flutter/material.dart';

// Package que permite a persistência de dados na memória do aparelho
import 'package:shared_preferences/shared_preferences.dart';

import 'models/item.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(),
    );
  }
}

// ignore: must_be_immutable
class HomePage extends StatefulWidget {
  var items = new List<Item>();

  // Construtor da classe, permitindo instancia de outras classes como models e execução de funções.
  HomePage() {
    items = [];
  }

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  /*
    controller das propriedades do texto; permite a manipulação do texto de mane-
    ira dinâmica através de funções externas como demostrado no metodo add(0 abaixo);
    isso permite a manipulação do texto em outros componentes como o FloatingActionButton
  */
  var newTaskCtrl = TextEditingController();

  void add() {
    if (newTaskCtrl.text.isEmpty) return;
    setState(() {
      widget.items.add(Item(title: newTaskCtrl.text, done: false));
      newTaskCtrl.clear();
    });
    save();
  }

  void remove(index) {
    setState(() {
      widget.items.removeAt(index);
    });
    save();
  }

  Future loadData() async {
    // Carrega os dados do disco do aparelho
    var prefs = await SharedPreferences.getInstance();

    // Lê os dados carregados, dependendo do tipo de dado a ser carregado, é usado uma função get diferente.
    var data = prefs.getString('data');

    if (data != null) {
      Iterable decodedList = jsonDecode(
          data); // Decodificação em formado json dos dados, fazendo ele ser iterável.

      // Para cada item da lista decodificada "decodedList", atribua na lista "result" os itens em formato de lista.
      List<Item> result = decodedList.map((x) => Item.fromJson((x))).toList();
      setState(() {
        widget.items = result;
      });
    }
  }

  save() async {
    var prefs = await SharedPreferences.getInstance();
    await prefs.setString('data', jsonEncode(widget.items));
  }

  _HomePageState() {
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    // context seria a "situação atual" da aplicação, no caso a página que está sendo renderizada.
    return Scaffold(
      appBar: AppBar(
        title: TextFormField(
          controller: newTaskCtrl, // Atribuindo o controle do texto para nós.
          keyboardType: TextInputType.text,
          style: TextStyle(color: Colors.white, fontSize: 24),
          decoration: InputDecoration(
              labelText: "Nova Tarefa",
              labelStyle: TextStyle(color: Colors.white38)),
        ),
      ),
      body: ListView.builder(
          itemCount: widget.items
              .length, // Você pode acessar os estados (State) através da palavra chave "widget".
          itemBuilder: (BuildContext ctx, int index) {
            final item = widget.items[index];
            return Center(
                child: Container(
                    // Calsse container possui atributos de estilização úteis como padding.
                    padding: EdgeInsets.all(20.0),
                    child: Dismissible(
                      // Widget que permite a ação de "draggintg".
                      key: Key(item.title), // Key é uma classe genérica .
                      direction: item.done == true
                          ? DismissDirection.endToStart
                          : null,
                      background: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Expanded(
                              child: Container(
                            color: Colors.red,
                            padding: EdgeInsets.all(8.0),
                            child: Text("Excluir",
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.white,
                                )),
                          ))
                        ],
                      ),
                      onDismissed: (direction) {
                        // Chamado quando o componente é deslizado.
                        // direction: parâmetro que permite o controle da direção.
                        if (direction == DismissDirection.endToStart) {
                          remove(index);
                        }
                      },
                      child: CheckboxListTile(
                        title: Text(item.title),
                        value: item.done,
                        onChanged: (value) => {
                          setState(() {
                            item.done = value;
                          }),
                          save()
                        },
                      ),
                    )));
          }), // ListView.builder() realiza renderização de itens sobre demanda.
      floatingActionButton: FloatingActionButton(
        onPressed: add,
        child: Icon(Icons.add),
        backgroundColor: Colors.purple[200],
      ),
    );
  }
}
